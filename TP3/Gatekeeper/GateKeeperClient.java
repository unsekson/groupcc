package GateKeeper3;

import java.util.ArrayList;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class GateKeeperClient extends Thread {
	

	String server;
	int port;
	int containerPort;
	int tId = -1;

	static int NUMBER_OF_USERS = 100; //threads
	static int NUMBER_OF_FIRST_READ = 5;   //
	static int NUMBER_OF_SECOND_READ = 100;   //
	static int NUMBER_OF_WRITE = 5;   //

	public GateKeeperClient(String server, int port, int thId) {
		this.server = server;
		this.port = port;
		this.tId = thId;
	}

	static long startTime = System.currentTimeMillis();
	public static void main(String[] args) {
		
		String server = "ec2-52-36-22-158.us-west-2.compute.amazonaws.com";
		int port = 5252;
		
		final GateKeeperClient[] threads = new GateKeeperClient[NUMBER_OF_USERS];
		
		ClientAdapterHandler.TOTAL_REQUESTS = NUMBER_OF_USERS *
				( NUMBER_OF_FIRST_READ + NUMBER_OF_SECOND_READ + NUMBER_OF_WRITE);
		startTime = System.currentTimeMillis();
		for (int a = 0; a < NUMBER_OF_USERS; ++a) {
			threads[a] =  new GateKeeperClient(server, port, a);
			threads[a].start();
			
			 try { Thread.sleep(100); } catch (InterruptedException e) {e.printStackTrace(); }
			 
		}
	}

	private Bootstrap bootstrap = null;
	private static EventLoopGroup group = new NioEventLoopGroup();
	private Channel channel = null;
	
	public void run() {
		try {
			if(bootstrap==null){
				final ClientAdapterHandler aNewClientAdaptHandler = new ClientAdapterHandler(group);
				bootstrap = new Bootstrap().group(group)
						.channel(NioSocketChannel.class)
						.handler(new ChannelInitializer<SocketChannel>() {
							@Override
							protected void initChannel(SocketChannel channel)
									throws Exception {
								ChannelPipeline pipeline = channel.pipeline();
								pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,Delimiters.lineDelimiter()));
								pipeline.addLast("decoder", new StringDecoder());
								pipeline.addLast("encoder", new StringEncoder());
								pipeline.addLast("handler", aNewClientAdaptHandler);
							}
						});
			}
			channel = bootstrap.connect(server, port).sync().channel();

			System.out.println("sending from " + this.tId);

			for (int a = 0; a < NUMBER_OF_FIRST_READ; ++a) {
				channel.writeAndFlush("select release_year from film where film_id = 500;\n");
			}
			Thread.sleep(5000);

			for (int a = 0; a < NUMBER_OF_SECOND_READ; ++a) {
				channel.writeAndFlush("select release_year from film where film_id = 500;\n");
			}
			Thread.sleep(5000);

			for (int a = 0; a < NUMBER_OF_WRITE; ++a) {
				channel.writeAndFlush(" insert into film (title, description, release_year, language_id) "
			            + " VALUES ('sample_movie', 'This is just a test', 2016, 1);\n");
			}
			//System.out.println("closing channel ...");
			//channel.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
			if(channel!=null){
				channel.close();
				channel.closeFuture();
				group.shutdownGracefully();
			}
		}
	}
}

class ClientAdapterHandler extends SimpleChannelInboundHandler<String> {
	
	private static final Object lck = new Object();
	public static int TOTAL_REQUESTS   = 0;
	public static int receivedResponse = 0;
	private static EventLoopGroup evtG = null;
	
	public ClientAdapterHandler(EventLoopGroup evt) {
		if(evtG == null){
			evtG = evt;
		}
	}
	
	private static final ArrayList<Channel> stacks = new ArrayList<>();
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		
		System.out.println(msg);
		synchronized (lck) {
			stacks.add(ctx.channel());
			++receivedResponse;
			System.out.println("receivedResponse: " + receivedResponse + " TOTAL_REQUESTS: " + TOTAL_REQUESTS);
			if(receivedResponse == TOTAL_REQUESTS) {
				for(Channel ch: stacks){
					ch.close();
					ch.closeFuture();
				}
				evtG.shutdownGracefully();
				long endTime   = System.currentTimeMillis();
				long totalTime = endTime -  GateKeeperClient.startTime;
				System.out.println(totalTime);
			}
		}
	}
}