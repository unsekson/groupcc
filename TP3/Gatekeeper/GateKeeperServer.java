package GateKeeper3;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class GateKeeperServer extends Thread {

    int port = 5252;

    public static void main(String[] args) {
    	System.out.println("calling GateKeeperServer().start() ...");
        new GateKeeperServer().start();
    }

    public void run() {
        port = 5252;
        EventLoopGroup producer = new NioEventLoopGroup();
        EventLoopGroup consumer = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap()
                    .group(producer, consumer)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>(){
                    	@Override
                	    protected void initChannel(SocketChannel channel) throws Exception {
                	        ChannelPipeline pipeline = channel.pipeline();
                	        pipeline.addLast("framer",new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
                	        pipeline.addLast("decoder", new StringDecoder());
                	        pipeline.addLast("encoder", new StringEncoder());
                	        pipeline.addLast("handler", new ServerAdapterHandler());
                	    }
                    }).option(ChannelOption.SO_BACKLOG, 1000);          
            
            System.out.println("GateKeeper Server has been started and is listening to port " + port + " ...");
            bootstrap.bind(port).sync().channel().closeFuture().sync();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.shutdownGracefully();
            consumer.shutdownGracefully();
        }
    }
}

class ServerAdapterHandler extends SimpleChannelInboundHandler<String> {
	
	private static EventLoopGroup group = new NioEventLoopGroup();
	public final static BlockingQueue<Channel> queue = new LinkedBlockingQueue<Channel>();
	private Channel trustedHostChannel = null;
	
	Bootstrap bootstrap = null;
	
	/*static {
		bootstrap = new Bootstrap().group(group)
				.channel(NioSocketChannel.class)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel channel)
							throws Exception {
						ChannelPipeline pipeline = channel.pipeline();
						pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,Delimiters.lineDelimiter()));
						pipeline.addLast("decoder", new StringDecoder());
						pipeline.addLast("encoder", new StringEncoder());
						pipeline.addLast("handler", new THClientAdapterHandler());
					}
				});
		try {
			trustedHostChannel = bootstrap.connect("172.31.15.241", 6565).sync().channel();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}*/
	
	public ServerAdapterHandler() {
		createNewChannel();
	}
	
	private void createNewChannel(){
		bootstrap = new Bootstrap().group(group)
				.channel(NioSocketChannel.class)
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					protected void initChannel(SocketChannel channel)
							throws Exception {
						ChannelPipeline pipeline = channel.pipeline();
						pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,Delimiters.lineDelimiter()));
						pipeline.addLast("decoder", new StringDecoder());
						pipeline.addLast("encoder", new StringEncoder());
						pipeline.addLast("handler", new THClientAdapterHandler());
					}
				});
		try {
			trustedHostChannel = bootstrap.connect("172.31.38.129", 6565).sync().channel();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static final Object lck = new Object();
	
	//results from GateKeeper Clients would be received here ...
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String queryFromGateKeeperClient) throws Exception {
        Channel currentChannel = ctx.channel();
        //Thread.sleep(100);
        trustedHostChannel.writeAndFlush(queryFromGateKeeperClient + "\n"); //send it to TrustedHost!
        queue.add(currentChannel);
        /*synchronized (lck) {
        	do{
        		String lastResult = queue.take();
        		currentChannel.writeAndFlush(lastResult);
        	}while(queue.size() > 0);
        	//currentChannel.writeAndFlush("OK" + "\n");
        	//trustedHostChannel.closeFuture();
		}*/
    }
    
//results from TrustedHost would be received here ...
static class THClientAdapterHandler extends SimpleChannelInboundHandler<String> {
	
	private static final Map<String, Integer> problematicQueries = new HashMap<String, Integer>();
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		Channel channel = queue.take();
		//do{
		String result = msg.split(", query:")[0];
		String querySent = msg.split(", query:")[1];
		
		if(problematicQueries.containsKey(querySent)){
	        int n = problematicQueries.get(querySent);
	        if(n >= 10){
	        	//ServerAdapterHandler.queue.add(querySent + " is invalid!" + "\n");
	        	channel.writeAndFlush(querySent + " is invalid!" + "\n");
	        	return;
	        }
	        if(result == "NULL"){
	        	n++;
	        	problematicQueries.put(querySent, n);
	        }
	       }else{
	    	   problematicQueries.put(querySent, 0);
	    }
		//ServerAdapterHandler.queue.add(result + "\n");
		channel.writeAndFlush(result + "\n");
		//}while(!queue.isEmpty());
	}
}
}