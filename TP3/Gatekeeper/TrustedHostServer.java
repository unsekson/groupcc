package TrustedHost;

import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.mysql.jdbc.Connection;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class TrustedHostServer extends Thread {

	int port;

	public static void main(String[] args) {
		new TrustedHostServer().start();
	}

	public void run() {
		port = 6565;
		EventLoopGroup producer = new NioEventLoopGroup();
		EventLoopGroup consumer = new NioEventLoopGroup();

		try {
			ServerBootstrap bootstrap = new ServerBootstrap()
					.group(producer, consumer)
					.channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel)
								throws Exception {
							ChannelPipeline pipeline = channel.pipeline();
							pipeline.addLast("framer",
									new DelimiterBasedFrameDecoder(8192,
											Delimiters.lineDelimiter()));
							pipeline.addLast("decoder", new StringDecoder());
							pipeline.addLast("encoder", new StringEncoder());
							pipeline.addLast("handler",
									new ServerAdapterHandler());
						}
					}).option(ChannelOption.SO_BACKLOG, 1000 * 1000);

			System.out
					.println("Trusted-Host-Arash Server has been started and is listening to port "
							+ port + " ...");
			bootstrap.bind(port).sync().channel().closeFuture().sync();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			producer.shutdownGracefully();
			consumer.shutdownGracefully();
		}
	}
}

class ServerAdapterHandler extends SimpleChannelInboundHandler<String> {

	/*
	 * private final static String slave1Ip =
	 * "ec2-54-186-156-235.us-west-2.compute.amazonaws.com"; private static
	 * final String slave1Address = String.format("jdbc:mysql://%s:%d/%s",
	 * slave1Ip , 3306, "sakila"); private static Connection slave1Connection =
	 * null; static{ try { Class.forName("com.mysql.jdbc.Driver").newInstance();
	 * slave1Connection = (Connection)
	 * DriverManager.getConnection(slave1Address, "root", ""); } catch
	 * (Exception e) { e.printStackTrace(); } }
	 */

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		QueueHelper.getInstance().add(new QueueObject(ctx.channel(), msg));
		QueueHelper.getInstance().process();
	}
	
	/*
	 * Thread.sleep(1000); Channel currentChannel = ctx.channel(); Statement
	 * statement = DbHelper.getConnection().createStatement(); try {
	 * if(msg.toLowerCase().contains("select")){ statement.executeQuery(msg);
	 * }else if(msg.toLowerCase().contains("insert")){
	 * statement.executeUpdate(msg); } currentChannel.writeAndFlush("result:OK"
	 * + ", query:" + msg + "\n"); } catch (Exception e) { e.printStackTrace();
	 * currentChannel.writeAndFlush("result:" + "NULL" + ", query:" + msg +
	 * "\n"); } statement.close(); }
	 */
	
}

class QueueObject implements Serializable, Comparable<QueueObject>,
		Comparator<QueueObject> {
	private Channel currChannel;
	private String msg;

	public QueueObject(Channel ch, String str) {
		this.currChannel = ch;
		this.msg = str;
	}

	public Channel getCh() {
		return this.currChannel;
	}

	public String getMsg() {
		return this.msg;
	}

	@Override
	public int compare(QueueObject o1, QueueObject o2) {
		return 0;
	}

	@Override
	public int compareTo(QueueObject o) {
		return 0;
	}
}

class QueueHelper {
	private static QueueHelper instance = new QueueHelper();
	private static final PriorityBlockingQueue<QueueObject> queue = new PriorityBlockingQueue<>();
	Thread[] consumers = new Thread[10];

	public QueueHelper() {
		for (int a = 0; a < 10; ++a) {
			consumers[a] = new Thread(new QueueConsumer(queue));
		}
	}

	public void add(QueueObject queueObj) {
		queue.add(queueObj);
	}

	public static QueueHelper getInstance() {
		if (instance == null) {
			instance = new QueueHelper();
		}
		return instance;
	}

	public void process() {
		if (!queue.isEmpty()) {
			for (Thread thread : consumers) {
				if (thread.getState() == Thread.State.NEW
						|| thread.getState() == Thread.State.TERMINATED) {
					thread = new Thread(new QueueConsumer(queue));
					thread.start();
				}
			}
		}
	}
}

class QueueConsumer implements Runnable {

	private final BlockingQueue<QueueObject> queue;

	public QueueConsumer(PriorityBlockingQueue<QueueObject> incomingQueue) {
		this.queue = incomingQueue;
	}

	public void run() {
		while (!queue.isEmpty()) {
			Channel currentChannel = null;
			String msg = "";
			try {
				QueueObject obj = queue.take();
				currentChannel = obj.getCh();
				msg = obj.getMsg();
				Statement statement = DbHelper.getConnection()
						.createStatement();
				if (msg.toLowerCase().contains("select")) {
					statement.executeQuery(msg);
				} else if (msg.toLowerCase().contains("insert")) {
					statement.executeUpdate(msg);
				}
				currentChannel.writeAndFlush("result:OK" + ", query:" + msg
						+ "\n");
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				if (currentChannel != null)
					currentChannel.writeAndFlush("result:" + "NULL"
							+ ", query:" + msg + "\n");
			}
		}
	}

}

class DbHelper {

	private final static String slave1Ip = "172.31.31.11";// "ec2-54-186-156-235.us-west-2.compute.amazonaws.com";
	private static final String slave1Address = String.format(
			"jdbc:mysql://%s:%d/%s", slave1Ip, 3306, "sakila");

	public static Connection slave1Connection = null;

	public static synchronized Connection getConnection() {
		if (slave1Connection == null) {
			try {
				slave1Connection = (Connection) DriverManager.getConnection(
						slave1Address, "root", "toor");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return slave1Connection;
	}
}
