package CompetitionConsumers;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.UUID;

import GateKeeper3.GateKeeperClient;

public class CompetConsumerClient extends Thread {

	String server;
	int port;
	int containerPort;
	int tId = -1;

	static int NUMBER_OF_USERS = 1000; //threads
	static int NUMBER_OF_INSERT = 5;   // 

	public CompetConsumerClient(String server, int port, int thId) {
		this.server = server;
		this.port = port;
		this.tId = thId;
	}
	static long startTime = System.currentTimeMillis();
	public static void main(String[] args) {
		String server =  "ec2-52-35-3-88.us-west-2.compute.amazonaws.com";
		int port = 6262;
		
		ClientAdapterHandler.TOTAL_REQUESTS = NUMBER_OF_USERS * NUMBER_OF_INSERT;
		
		final CompetConsumerClient[] threads = new CompetConsumerClient[NUMBER_OF_USERS];
		startTime = System.currentTimeMillis();
		for (int a = 0; a < NUMBER_OF_USERS; ++a) {
			threads[a] =  new CompetConsumerClient(server, port, a);
			threads[a].start();
		}
	}

	private EventLoopGroup group = new NioEventLoopGroup();
	public void run() {
		Channel channel = null;
		try {
			final ClientAdapterHandler aNewClientAdaptHandler = new ClientAdapterHandler(group);
			Bootstrap bootstrap = new Bootstrap().group(group)
					.channel(NioSocketChannel.class)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel)
								throws Exception {
							ChannelPipeline pipeline = channel.pipeline();
							pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192,Delimiters.lineDelimiter()));
							pipeline.addLast("decoder", new StringDecoder());
							pipeline.addLast("encoder", new StringEncoder());
							pipeline.addLast("handler", aNewClientAdaptHandler);
						}
					});

			channel = bootstrap.connect(server, port).sync().channel();
			
			System.out.println("sending from " + this.tId);

			for (int a = 0; a < NUMBER_OF_INSERT; ++a) {
				//make sure the GlassFish Server is running ...
				Object res = "";
				UUID uid = MQRestCaller.PlaceRequest("placeIntoMessageQueueRandomProxy");
	            res = MQRestCaller.GetFromMessageQueue(uid);
				String uuid = uid.toString() + res.toString();
				String insertQuery = uuid + "SEP"+"insert into film (title, description, release_year, language_id) "
			            + "VALUES ('sample_movie', 'This is just a test', 2016, 1);";
				channel.writeAndFlush(String.format("%s\n", insertQuery));
			}
			Thread.sleep(1000);
		} 
		catch (Exception e) {
			e.printStackTrace();
			if(channel!=null){
				channel.close();
				channel.closeFuture();
			}
		} 
	}
}

class ClientAdapterHandler extends SimpleChannelInboundHandler<String> {
	public static int TOTAL_REQUESTS   = 0;
	public static int receivedResponse = 0;
	private EventLoopGroup evtG = null;
	public ClientAdapterHandler(EventLoopGroup evt) {
		this.evtG = evt;
	}
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
			throws Exception {
		System.out.println(msg);
		++receivedResponse;
		System.out.println("receivedResponse: " + receivedResponse);
		if(receivedResponse == TOTAL_REQUESTS) {  //important: set to total number of request and response ...
			evtG.shutdownGracefully();
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime -  CompetConsumerClient.startTime;
			System.out.println(totalTime);
		}
	}
}