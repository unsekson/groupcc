package CompetitionConsumers;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;
import java.sql.Statement;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class CompConsServer extends Thread {

    int port;

    public static void main(String[] args) {
        new CompConsServer().start();
    }

    public void run() {
        port = 6262;
        EventLoopGroup producer = new NioEventLoopGroup();
        EventLoopGroup consumer = new NioEventLoopGroup();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap()
                    .group(producer, consumer)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>(){
                    	@Override
                	    protected void initChannel(SocketChannel channel) throws Exception {
                	        ChannelPipeline pipeline = channel.pipeline();
                	        pipeline.addLast("framer",new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
                	        pipeline.addLast("decoder", new StringDecoder());
                	        pipeline.addLast("encoder", new StringEncoder());
                	        pipeline.addLast("handler", new ServerAdapterHandler());
                	    }
                    }).option(ChannelOption.SO_BACKLOG, 120*1000);          
            
            System.out.println("CompetingConsumer Server has been started and is listening to port " + port +" ...");
            bootstrap.bind(port).sync().channel().closeFuture().sync();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.shutdownGracefully();
            consumer.shutdownGracefully();
        }
    }
}

class ServerAdapterHandler extends SimpleChannelInboundHandler<String> {
	
 	private final static String slave1Ip = "172.31.40.191";
    private final static String slave2Ip = "172.31.40.192";
    private final static String slave3Ip = "172.31.40.189";
    
    private static final String slave1Address = String.format("jdbc:mysql://%s:%d/%s", slave1Ip , 3306, "sakila");
    private static final String slave2Address = String.format("jdbc:mysql://%s:%d/%s", slave2Ip, 3306, "sakila");
    private static final String slave3Address = String.format("jdbc:mysql://%s:%d/%s", slave3Ip, 3306, "sakila");
	    
	private static Connection slave1Connection = null;
    private static Connection slave2Connection = null;
    private static Connection slave3Connection = null;
    
    static{
    	
    	try {
    		System.out.println("Initializing Connections ...");
    		
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
	        slave1Connection = (Connection) DriverManager.getConnection(slave1Address, "root", "toor");
	        slave2Connection = (Connection) DriverManager.getConnection(slave2Address, "root", "toor");
	        slave3Connection = (Connection) DriverManager.getConnection(slave3Address, "root", "toor");
	        
		} catch (Exception e) {
		}
    }
	
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        Channel currentChannel = ctx.channel();
        //execute the query and return the result by the line below. Instead of the message the result should be written!
        //execute query based on an algorithm to hit all the slave servers!!!! ...
        String uid = msg.split("SEP")[0];
        String query = msg.split("SEP")[1];
        
        System.out.println(uid);
        int middleDigit = Integer.parseInt(uid.toString().split("-")[2].substring(1,4), 16);
		 
        Statement statement = null;
		 //with substring(1,4);
		 if(middleDigit<=1363) {
			 statement = slave3Connection.createStatement();
		 }
		 else if (middleDigit>1363 && middleDigit <=2726) {
			 statement = slave2Connection.createStatement();
		 }
		 else if(middleDigit>2726) {
			 statement = slave1Connection.createStatement();
		 }
		 //because we know it is always insert:
		 currentChannel.writeAndFlush(statement.executeUpdate(query) + "\n");
    }
}