#!/bin/bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential



#CPU installation
wget http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz 
tar xvf UnixBench5.1.3.tgz
cd UnixBench
make
cd ..
#IOping installation
sudo apt-get install ioping
#Redis installation
wget http://download.redis.io/releases/redis-3.0.6.tar.gz
tar xzf redis-3.0.6.tar.gz
cd redis-3.0.6
make
cd ..
sudo apt-get install redis-tools
sudo apt-get install redis-server
#DBench
sudo apt-get install dbench
#speedTest
sudo apt-get install python-pip
sudo pip install speedtest-cli
#Sysbench
sudo apt-get install sysbench


#______________________________________________________

#1-CPU
cd UnixBench
for i in `seq 1 5`
do
sudo ./Run > UnixBench.txt
grep 'System Benchmarks Index Score' UnixBench.txt >> UnixBenchScore.txt
done
cd ..

for i in `seq 1 5`
do

#2-IO
#IF : PATH of the INPUT that contains data to be copied.
#OF : PATH of the output filesystem.
#BS: Block size
#COUNT : Number of loops for the amount of blocks to write.
#CONV : ask dd command to report the speed only after the data is synced with the disk, because without the CONV attribute, the speed that dd reported to us is the speed with which data was cached to RAM memory

sudo dd if=/dev/zero of=sb-io-test bs=1MB count=1k conv=fdatasync &>> io.txt

#3-ioping
sudo ioping -RL /dev/sda >> ioping.txt
#xvda for AWS

#4-redis
cd redis-3.0.6
#number of SET and GET requests per second with pipeline
sudo redis-benchmark -n 1000000 -t set,get -P 100 -q > redis.txt
grep 'SET' redis.txt >> redisSet.txt
#memory fragmentation
#sudo redis-cli info memory
cd ..

#5-Disk performance
dbench 50 > dbench.txt
grep 'Throughput' dbench.txt >> dbenchthroughput.txt
#speed of a disk
#The -t Option will show us the speed of reading from the cache buffer
#The -T option will show you the speed of reading without pre-cached buffer.
#sudo hdparm -Tt /dev/sda

#6-speed test
speedtest-cli > speedtest.txt
grep 'Download:' speedtest.txt >> downloadspeedtest.txt

done

for i in `seq 1 5`
do
#7-sysbench CPU test
sysbench --test=cpu --cpu-max-prime=100000 run > sysbench.txt
grep 'total time:' sysbench.txt >> sysbenchTotalTime.txt
done