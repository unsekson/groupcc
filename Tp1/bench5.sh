#!/bin/bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential



#CPU installation
wget http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz 
tar xvf UnixBench5.1.3.tgz
cd UnixBench
make
cd ..
#IOping installation
sudo apt-get install ioping
#Redis installation
wget http://download.redis.io/releases/redis-3.0.6.tar.gz
tar xzf redis-3.0.6.tar.gz
cd redis-3.0.6
make
cd ..
sudo apt-get install redis-tools
sudo apt-get install redis-server
#DBench
sudo apt-get install dbench


#______________________________________________________

#1-CPU
cd UnixBench
for i in `seq 1 5`
do
sudo ./Run >> UnixBench.txt
done
cd ..

for i in `seq 1 5`
do

#2-IO
sudo dd if=/dev/zero of=sb-io-test bs=1MB count=1k conv=fdatasync &>> io.txt

#3-ioping
sudo ioping -RL /dev/xvda >> ioping.txt
#xvda for AWS

#4-redis
cd redis-3.0.6
#number of SET and GET requests per second with pipeline
sudo redis-benchmark -n 1000000 -t set,get -P 100 -q >> redis.txt
#memory fragmentation
#sudo redis-cli info memory
cd ..

#5-Disk performance
dbench 20 >> dbench.txt
#speed of a disk
#The -t Option will show us the speed of reading from the cache buffer
#The -T option will show you the speed of reading without pre-cached buffer.
#sudo hdparm -Tt /dev/sda

#sudo tbench_srv

done
